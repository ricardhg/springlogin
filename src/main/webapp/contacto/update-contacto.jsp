
<%@ page language="java" contentType="text/html;charset=UTF-8" %>


<%@ page import = "org.springframework.validation.*" %> 

<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>

<%@include file="../parts/head.html" %>
<%@include file="../parts/nav.html" %>


<div class="container">

  <div class="row">
    <div class="col-md-6">
    <br>
    <br>
        <h1>Editar contacte</h1>
    <br>

     <form:form action="${pageContext.request.contextPath}/contacto/update/${contacto.id}" method="post" modelAttribute="contacto">
       <spring:message code="lbl.nombre" text="Nom" />
        <form:input path="nombre" class="form-control" />
        <form:errors path="nombre" cssClass="error" />
        <br>
        <spring:message code="lbl.email" text="Email" />
        <form:input path="email" class="form-control" />
        <form:errors path="email" cssClass="error" />
        <br>
        <button class="btn btn-success" type="submit" value="">Desar</button> 
      </form:form>
    <div>
  <div>
<div>



<%@include file="../parts/foot.html" %>




