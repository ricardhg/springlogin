
<%@ page language="java" contentType="text/html;charset=UTF-8" %>

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>  


<%@include file="../parts/head.html" %>
<%@include file="../parts/nav.html" %>


<div class="container">

  <div class="row">
    <div class="col-md-6">
    <br>
    <br>
        <h1>Mostrar contracte</h1>
    <br>

    <h5>Nombre</h5>
    <h4><c:out value="${contacto.nombre}" /></h4>


    <table class="table">
    <thead>
    <tr>
    <th>#</th>
    <th>Notas</th>

    </tr>
    </thead>
    <tbody>

        <c:forEach items="${contacto.llamadas}" var="llamada">
            <tr>
                <td>${llamada.id}</td>
                <td><c:out value="${llamada.notas}" /></td>
  
            </tr>
        </c:forEach>

    </tbody>
    </table>

  <div>
<div>



<%@include file="../parts/foot.html" %>




