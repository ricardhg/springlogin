<%@ page language="java" contentType="text/html;charset=UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>  


<%@include file="../parts/head.html" %>
<%@include file="../parts/nav.html" %>

<div class="container">
<br>
<br>
 <h1>Contactes</h1>
<br>

    <table class="table">
    <thead>
    <tr>
    <th>#</th>
    <th>Nom</th>
    <th>Email</th>
    <th>Qtt</th>
    <th></th>
    <th></th>
    <th></th>
    <th></th>
    </tr>
    </thead>
    <tbody>

        <c:forEach items="${contactos}" var="contacto">
            <tr>
                <td>${contacto.id}</td>
                <td><c:out value="${contacto.nombre}" /></td>
                <td><c:out value="${contacto.email}" /></td>
               <td><c:out value="${contacto.llamadas.size()}" /></td>
                <td><a href="/contacto/edit/${contacto.id}">Editar</a></td>
                <td><a href="/contacto/show/${contacto.id}">Mostrar</a></td>
                <td><a href="/contacto/delete/${contacto.id}">Eliminar</a></td>
                <td><a href="/llamada/addllamada/${contacto.id}">Nueva llamada</a></td>
            </tr>
        </c:forEach>

    </tbody>
    </table>
    <br />
    
    <a href="/contacto/addcontacto" class="btn btn-success" >Nou contacte</a> 

<div>

<%@include file="../parts/foot.html" %>


