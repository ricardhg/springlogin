<%@ page language="java" contentType="text/html;charset=UTF-8" %>


<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
 



<%@include file="../parts/head.html" %>
<%@include file="../parts/nav.html" %>

<div class="container">

  <div class="row">
    <div class="col-md-6">
    <br>
    <br>
        <h1>Afegir trucada per ${contacto.nombre}</h1>
    <br>

      <form:form action="#" method="post" modelAttribute="llamada">
        <spring:message code="lbl.notas" text="Comentari" />
        <form:input path="notas" class="form-control" />
        <form:errors path="notas" cssClass="error" />
        <br>
        <button class="btn btn-success" type="submit" value="">Desar</button> 
      </form:form>
    <div>
  <div>
<div>

<%@include file="../parts/foot.html" %>




