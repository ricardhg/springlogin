package com.enfocat.springlogin.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "llamadas")
public class Llamada {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;

    private String notas;
   

    @ManyToOne
    @JoinColumn(name = "idcontacto", nullable = false)
    private Contacto contacto;

    // CONSTRUCTOR PELAT
    public Llamada() {
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getNotas() {
        return notas;
    }

    public void setNotas(String notas) {
        this.notas = notas;
    }


    public Contacto getContacto() {
        return contacto;
    }

    public void setContacto(Contacto contacto) {
        this.contacto = contacto;
    }

    @Override
    public String toString() {
        return this.notas;
    }

}