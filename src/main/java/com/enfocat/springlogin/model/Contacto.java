package com.enfocat.springlogin.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotBlank;

import java.util.Set;


@Entity
@Table(name="contactos")
public class Contacto {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;

    @NotBlank(message = "Te has dejado el nombre")
    private String nombre;


    private String email;
    private String ciudad;
    private String urlfoto;
    private String piefoto;


    @OneToMany(mappedBy="contacto")
    private Set<Llamada> llamadas;


    
    //CONSTRUCTOR PELAT
    public Contacto(){}

   
   
    @Override
    public String toString() {
        return String.format("%s (%s)", this.nombre, this.email);
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }


    
    public String getUrlfoto(){
        return this.urlfoto;
    }
    
    
    public String getUrlfotoPath(){
        String base = "/contactos/uploads/";
        if (this.urlfoto==null || this.urlfoto.length()==0) {
            return base + "nofoto.png";
        } else {
            return base + this.urlfoto;
        }
    }

    protected void setUrlfoto(String urlfoto){
        this.urlfoto = urlfoto;
    }

    public String getPiefoto() {
        return piefoto;
    }

    public void setPiefoto(String piefoto) {
        this.piefoto = piefoto;
    }

    public String getCiudad() {
        return ciudad;
    }

    public void setCiudad(String ciudad) {
        this.ciudad = ciudad;
    }

    public Set<Llamada> getLlamadas() {
        return llamadas;
    }

    public void setLlamadas(Set<Llamada> llamadas) {
        this.llamadas = llamadas;
    }

  
}