package com.enfocat.springlogin.web;

import com.enfocat.springlogin.model.Llamada;
import com.enfocat.springlogin.repository.LlamadaRepository;
import com.enfocat.springlogin.model.Contacto;
import com.enfocat.springlogin.repository.ContactoRepository;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.Optional;

@Controller
@RequestMapping(path = "/llamada")
public class LlamadaController {

    private final LlamadaRepository llamadaRepository;
    private final ContactoRepository contactoRepository;

    @Autowired
    public LlamadaController(LlamadaRepository llamadaRepository, ContactoRepository contactoRepository) {
        this.llamadaRepository = llamadaRepository;
        this.contactoRepository = contactoRepository;
    }

    @GetMapping("/addllamada/{idcontacto}")
    public String addLlamada(@PathVariable(value = "idcontacto") Long idContacte, Model model, Llamada llamada) {
        Contacto contacto = contactoRepository.findById(idContacte).get();
        model.addAttribute("contacto", contacto);
        return "llamada/add-llamada";
    }

    @PostMapping("/addllamada/{idcontacto}")
    public String addLlamada(@PathVariable(value = "idcontacto") Long idContacte, @Valid Llamada llamada,
            BindingResult result, Model model) {
        if (result.hasErrors()) {
            return "add-llamada";
        }
        Contacto contacto = contactoRepository.findById(idContacte).get();
        llamada.setContacto(contacto);
        llamadaRepository.save(llamada);
        model.addAttribute("contactos", contactoRepository.findAll());
        return "contacto/contacto-list";
    }

}