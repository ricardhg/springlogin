package com.enfocat.springlogin.web;


import com.enfocat.springlogin.model.Contacto;
import com.enfocat.springlogin.repository.ContactoRepository;


import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.Optional;


@Controller
@RequestMapping(path="/contacto")
public class ContactoController {
    
    private final ContactoRepository contactoRepository;

    @Autowired
    public ContactoController(ContactoRepository contactoRepository) {
        this.contactoRepository = contactoRepository;
    }


    @GetMapping(path="/id")
    public @ResponseBody Optional<Contacto> getIdParam(@RequestParam String id){
        return contactoRepository.findById(Long.parseLong(id));
    }



    @GetMapping("/all")
    public String getAll(Model model){
        model.addAttribute("contactos", contactoRepository.findAll());
        return "contacto/contacto-list";
    }

    @GetMapping("/alljson")
    public @ResponseBody Iterable<Contacto> getAllJSON(){
        return contactoRepository.findAll();
    }

    @GetMapping("/addcontacto")
    public String addContacto(Contacto contacto) {
        return "contacto/add-contacto";
    }

     @PostMapping("/addcontacto")
    public String addContacto(@Valid Contacto contacto, BindingResult result, Model model) {
        if (result.hasErrors()) {
            System.out.println("hi ha errors");
            return "add-contacto";
        }

        System.out.println("xxxxxx");

        contactoRepository.save(contacto);
        model.addAttribute("contactos", contactoRepository.findAll());
        return "contacto/contacto-list";
    }
    


    @PostMapping("/addcontactojson")
    public  @ResponseBody Contacto addContactoJSON( @RequestBody Contacto contacto) {
        return contactoRepository.save(contacto);
    }


        
    @PutMapping("/updatecontactojson/{id}")
    public @ResponseBody Contacto updateContactoJSON(      
        @PathVariable(value = "id") Long idContacte, @Valid @RequestBody Contacto contacteRebut) {
        Contacto contacto = contactoRepository.findById(idContacte).get();

        contacto.setEmail(contacteRebut.getEmail());
        contacto.setNombre(contacteRebut.getNombre());
        final Contacto updatedContacte = contactoRepository.save(contacto);
        return updatedContacte;
    }

    
    @GetMapping("/deletejson/{id}")
    public @ResponseBody String deleteContactojson(@PathVariable("id") long idContacte) {
        Contacto contacto = contactoRepository.findById(idContacte).get();
        contactoRepository.delete(contacto);
        return "deleted";
    }

    @GetMapping("/show/{id}")
    public String show(@PathVariable("id") long id, Model model) {
        Contacto contacto = contactoRepository.findById(id).orElseThrow(() -> new IllegalArgumentException("Invalid contacto Id:" + id));
        model.addAttribute("contacto", contacto);
        return "contacto/show-contacto";
    }

    @GetMapping("/edit/{id}")
    public String showUpdateForm(@PathVariable("id") long id, Model model) {
        Contacto contacto = contactoRepository.findById(id).orElseThrow(() -> new IllegalArgumentException("Invalid contacto Id:" + id));
        model.addAttribute("contacto", contacto);
        return "contacto/update-contacto";
    }
    
    @PostMapping("/update/{id}")
    public String updateContacto(@PathVariable("id") long id, @Valid Contacto contacto, BindingResult result, Model model) {
        if (result.hasErrors()) {
            contacto.setId(id);
            return "contacto/update-contacto";
        }
        
        contactoRepository.save(contacto);
        model.addAttribute("contactos", contactoRepository.findAll());
        return "contacto/contacto-list";
    }


    
    @GetMapping("/delete/{id}")
    public String deleteContacto(@PathVariable("id") long id, Model model) {
        Contacto contacto = contactoRepository.findById(id).orElseThrow(() -> new IllegalArgumentException("Invalid contacto Id:" + id));
        contactoRepository.delete(contacto);
        model.addAttribute("contactos", contactoRepository.findAll());
        return "contacto/contacto-list";
    }
}