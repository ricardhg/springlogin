package com.enfocat.springlogin.repository;

import com.enfocat.springlogin.model.Contacto;

import org.springframework.data.repository.CrudRepository;

public interface ContactoRepository extends CrudRepository<Contacto, Long> {
   
}
