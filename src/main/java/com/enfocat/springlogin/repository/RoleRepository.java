package com.enfocat.springlogin.repository;

import com.enfocat.springlogin.model.Role;
import org.springframework.data.jpa.repository.JpaRepository;

public interface RoleRepository extends JpaRepository<Role, Long>{
}
