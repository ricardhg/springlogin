package com.enfocat.springlogin.repository;

import com.enfocat.springlogin.model.User;
import org.springframework.data.jpa.repository.JpaRepository;

public interface UserRepository extends JpaRepository<User, Long> {
   
    User findByUsername(String username);
    
}
