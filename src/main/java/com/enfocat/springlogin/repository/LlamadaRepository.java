package com.enfocat.springlogin.repository;

import com.enfocat.springlogin.model.Llamada;

import org.springframework.data.repository.CrudRepository;

public interface LlamadaRepository extends CrudRepository<Llamada, Long> {
   
}
