package com.enfocat.springlogin.service;

import com.enfocat.springlogin.model.User;

public interface UserService {
    void save(User user);

    User findByUsername(String username);
}
